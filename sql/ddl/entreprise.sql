
CREATE TABLE "entreprise" (
    "id" SERIAL PRIMARY KEY,
    "adresse" TEXT,
    "code_pays" TEXT,
    "code_postal" TEXT,
    "entreprise_émmetrice" TEXT,
    "filiale_déclarante" TEXT,
    "identifiant_entreprise" TEXT,
    "pays" TEXT,
    "secteur" TEXT,
    "ville" TEXT
);
