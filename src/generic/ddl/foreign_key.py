from src.generic.ddl.abstract import DDL
from src.generic.ddl.postgres_entities import FOREIGN_KEY


class ForeignKeyDDL(DDL):
    DDL_ENTITY = FOREIGN_KEY
    NAME_TEMPLATE = "{table}_{column}_foreign_key_{parent_table}_{parent_column}"
    DDL_TEMPLATE = """
ALTER TABLE public."{table}"
ADD CONSTRAINT "{name}" FOREIGN KEY ("{column}") REFERENCES public."{parent_table}" ("{parent_column}");
"""
    DROP_TEMPLATE = """
ALTER TABLE public."{table}"
DROP CONSTRAINT IF EXISTS "{name}" CASCADE;
"""

    def __init__(self, table, column, parent_table, parent_column=None):
        super().__init__()
        self.table = table
        self.column = column
        self.parent_table = parent_table
        self.parent_column = parent_column or column
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)

    @property
    def query(self):
        return self.DDL_TEMPLATE.format(**self.__dict__)
