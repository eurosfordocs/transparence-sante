import re
import string
import unicodedata


def to_ascii_lowercase(any_str):
    no_accent = replace_accents(any_str)
    no_accent_lowercase = str.lower(no_accent)
    ascii_lowercase_spaces = get_non_authorized_chars_remover(string.ascii_lowercase)(no_accent_lowercase)
    return remove_extra_space(ascii_lowercase_spaces)


def get_non_authorized_chars_remover(authorized_chars, replacement_char=' '):
    def non_authorized_chars_remover(any_str):
        return re.sub('[^{}]'.format(authorized_chars + replacement_char), replacement_char, any_str)

    return non_authorized_chars_remover


def replace_accents(any_str):
    return ''.join(c for c in unicodedata.normalize('NFD', any_str)
                   if unicodedata.category(c) != 'Mn')


def remove_extra_space(any_str):
    return ' '.join(any_str.split())