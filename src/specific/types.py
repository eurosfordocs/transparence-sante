from typing import List, Union

from src.generic.ddl.foreign_key import ForeignKeyDDL
from src.generic.ddl.index import IndexDDL, SearchIndexDDL
from src.generic.ddl.materialized_view import MaterializedViewDDL
from src.generic.ddl.view import View
from src.generic.uploader import Uploader

index_list = List[Union[IndexDDL, SearchIndexDDL]]
view_list = List[Union[View, MaterializedViewDDL]]
upload_actions_list = List[Union[Uploader, View, MaterializedViewDDL, IndexDDL, SearchIndexDDL, ForeignKeyDDL]]