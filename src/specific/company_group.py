"""
Ce script permet de créer un fichier pour ajouter les nouvelles entreprises dans le fichier google sheets
de crowdsourcing de l'association aux groupes.
"""

import logging
from os.path import join

import pandas as pd

from src.constants.csv import COMPANY_CSV, COMPANY_GROUP_CSV, COMPANY_GROUP_MISSING_COMPANIES_CSV, COMPANY_GROUP_URL
from src.constants.directories import ROOTED_RAW_DATA_DIR, CLEANED_DATA_DIR
from src.generic.downloader import Downloader

company_group_downloader = Downloader(COMPANY_GROUP_URL, COMPANY_GROUP_CSV)


def add_missing_companies_to_group(reset=True):
    company_group_downloader.reset()
    company_group_downloader.run()

    logging.warning(
        "Cette opération nécessite d'avoir les dernières versions des fichiers "
        "(commandes `./main.py download -r` puis ./main.py extract -r`)")
    df_company = pd.read_csv(join(ROOTED_RAW_DATA_DIR, COMPANY_CSV))
    df_company_group = pd.read_csv(join(ROOTED_RAW_DATA_DIR, COMPANY_GROUP_CSV)) \
        .dropna(subset=['identifiant'])

    if set(df_company_group.identifiant) == set(df_company.identifiant):
        logging.info("Toutes les entreprises sont référencées dans l'association aux groupes.")
        return

    assert not df_company_group.identifiant.duplicated().sum()
    removed_company_id = set(df_company_group.identifiant) - set(df_company.identifiant)
    if removed_company_id:
        raise ValueError("Some company id have been removed : {}\n Delete rows in gsheet".format(removed_company_id))

    company_columns_to_keep = ['identifiant', 'secteur', 'denomination_sociale']
    company_group_columns_to_keep = ['identifiant',
                                     'Indépendant',
                                     'groupe',
                                     'sous_groupe',
                                     'Associé par',
                                     'Validé par',
                                     'Justification : source ou  commentaire'
                                     ]
    company_group_missing_path = join(CLEANED_DATA_DIR, COMPANY_GROUP_MISSING_COMPANIES_CSV)
    (df_company[company_columns_to_keep]
     .merge(df_company_group[company_group_columns_to_keep], on='identifiant', how='outer')
     .sort_values('denomination_sociale')
     .to_csv(company_group_missing_path, index=False)
     )
    logging.info(f"Certaines entreprises ne sont pas référencées dans l'association aux groupes.\n"
                 "Importer les valeurs du csv '{company_group_missing_path}' dans le tableau {COMPANY_GROUP_URL}.\n"
                 "Se placer sur la première cellule > Importer > Remplacer les données … > Virgule > Convertir le texte"
                 )


if __name__ == '__main__':
    company_group_downloader.reset()
    company_group_downloader.run()
    # add_missing_companies_to_group()
