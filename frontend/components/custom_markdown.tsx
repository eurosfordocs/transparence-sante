import Link from "next/link";
import React, {FunctionComponent} from "react";
import Markdown from "react-markdown/with-html";
import Col from "react-bootstrap/Col";
import {ReactMarkdownProps} from "react-markdown";


interface CustomLinkWithPrefetchProps {
    href: string;
    children: Element;
}

function CustomLinkWithPrefetch({href, children}: CustomLinkWithPrefetchProps) {
    if (href.startsWith('/')) {
        return (
            <Link href={href}><a>{children}</a></Link>
        );
    } else {
        return (
            <a href={href}>{children}</a>
        );
    }
}


function flatten(text: string, child: any): any {
    return typeof child === 'string'
        ? text + child
        : React.Children.toArray(child.props.children).reduce(flatten, text)
}

function slugifyHeader(text: string): string {
    return text.toLowerCase().replace(/\W/g, '-');
}

function HeadingRenderer(props: any) {
    const children = React.Children.toArray(props.children);
    const text = children.reduce(flatten, '');
    const slug = slugifyHeader(text);
    return React.createElement('h' + props.level, {id: slug}, props.children)
}

interface TocProps {
}

const Toc: FunctionComponent<TocProps> = ({children}) => {
    if (!children) {
        return (<div></div>);
    }
    const TOCLines = (children as any).reduce((acc: any, {key, props}: any) => {
        // Skip non-headings
        if (key.indexOf('heading') !== 0) {
            return acc;
        }

        // Indent by two spaces per heading level after h1
        let indent = '';
        for (let idx = 1; idx < props.level; idx++) {
            indent = `${indent}  `;
        }

        // Append line to TOC
        // This is where you'd add a link using Markdown syntax if you wanted
        const headerText = props.children[0].props.children;
        return acc.concat([
            `${indent}* [${headerText}](#${slugifyHeader(headerText)})`
            //{level: props.level, text: headerText, link: `#${slugifyHeader(headerText)}`}
        ]);
    }, []);

    return (
        <>
            <Col md={{span: 6, order: 1}} xs={{span: 12, order: 2}}>
                {children}
            </Col>
            <Col md={{span: 3, order: 2}} xs={{span: 12, order: 1}}>
                <Markdown className="ts-toc ts-sticky" source={TOCLines.join("\n")}/>
                <style jsx>{`
                    :global(.ts-toc ul) {
                        list-style: none;
                    }
                `}</style>
            </Col>
        </>
    );
};


interface CustomMarkdownProps extends ReactMarkdownProps {
    withToc?: boolean;
}

function CustomMarkdown(props: CustomMarkdownProps) {
    if (props.withToc) {
        return (
            <Markdown renderers={{link: CustomLinkWithPrefetch, root: Toc, heading: HeadingRenderer}}
                      escapeHtml={false} {...props}/>
        );
    } else {
        return (
            <Markdown renderers={{link: CustomLinkWithPrefetch, heading: HeadingRenderer}}
                      escapeHtml={false} {...props}/>
        );
    }
}

export default CustomMarkdown;
