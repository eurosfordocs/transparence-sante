import React from "react";
import ContentPage from "../components/content_page";

const pageText = `
## Un libre accès aux données…

Entre octobre 2013 et février 2015, l'association Regards Citoyens met en place un [outil de collecte](https://github.com/regardscitoyens/sunshine-data) des déclarations présentes sur la base Transparence-Santé, ainsi que de celles déposées sur les différents sites des ordres avant le lancement du site unique.  

Elle [publie](https://www.regardscitoyens.org/sunshine-ces-245-millions-deuros-que-les-laboratoires-pharmaceutiques-veulent-cacher/) ces données en libre accès en mars 2015, ainsi qu'une précieuse [vision d'ensemble](https://www.regardscitoyens.org/sunshine/).
Les données et analyses sont anonymisées pour respecter un [avis de la CNIL](https://www.regardscitoyens.org/lavis-de-la-cnil-qui-demande-la-confidentialite-des-declarations-publiques-dinterets/#r1). 

En janvier 2016, la loi est modifiée pour autoriser la publication des données de la base Transparence-Santé. 
En mai, elles sont mises à disposition [par Etalab](https://www.etalab.gouv.fr/la-base-transparence-sante-sur-data-gouv-fr) sur le site [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/transparence-sante-1/).

## … insuffisant pour les exploiter
 
Cependant, les données brutes sont trop volumineuses pour les logiciels d'analyse grand public tels qu'Excel.
De plus, leur médiocre qualité nécessite un important travail de nettoyage.

En pratique, les données ne sont pas exploitables sans compétences en programmation.

De fait, les médias ne profitent pas de cette ouverture pour effectuer des analyses globales sur le lobbying des industries de santé. 
Le journal _Le Monde_ fait exception, avec l'article 
[Comment l’argent des labos irrigue le monde de la santé](https://abonnes.lemonde.fr/economie/article/2017/10/12/les-laboratoires-aux-petits-soins-pour-les-medecins_5199912_3234.html), 
accompagné d'un autre exposant les difficultés techniques rencontrées : 
[Les ratés de la base de données publique Transparence Santé](https://abonnes.lemonde.fr/les-decodeurs/article/2017/10/12/les-rates-de-la-base-de-donnees-publique-transparence-sante_5199937_4355770.html).

## Simplifier l'utilisation de Transparence-Santé

Le projet EurosForDocs est né du décalage entre le potentiel de la base Transparence-Santé et son usage limité. 

L'objectif prioritaire est de simplifier l'accès aux données existantes, pour servir une large gamme d'utilisateurs et d'usage :

- patients - se renseigner sur les liens d'intérêts de leur médecin 
- citoyens - mieux comprendre l'influence de l'industrie sur le système de santé 
- professionnels de santé - se sensibiliser et participer à l'indépendance de la médecine
- administrations publiques - contrôler l'indépendance des experts sanitaires
- journalistes et chercheurs - mener aisément leurs investigations et recherches
- ordres professionnels - croiser avec les données de leur propre système informatique

Cette première étape doit démontrer la valeur de la transparence, et le caractère fallacieux de prétendues limites techniques. 

Un objectif sous-jacent est de pousser le ministère de la santé à améliorer le dispositif Transparence-Santé ; 
de la même façon que le projet [Dollars for Docs](https://projects.propublica.org/docdollars/) a conduit l'administration à [améliorer la restitution des données](https://twitter.com/eurosfordocs/status/1219049939264774144) aux États-Unis 

Suite au scandale du Levothyrox, [la ministre s'est engagée en septembre 2018](https://solidarites-sante.gouv.fr/actualites/presse/discours/article/discours-d-agnes-buzyn-remise-du-rapport-sur-l-amelioration-de-l-information) 
à _"sécuriser, moderniser et améliorer l'accessibilité de la base Transparence Santé en adaptant ses fonctionnalités, son ergonomie et l’exploitation des données qui peut en être faite"_, sans aucune conséquence pratique depuis.

Nous défendons une transparence totale, dans la même logique que _l'Ordre des médecins*_, ce qui suppose un réel contrôle des déclarations des industriels.

Plus fondamentalement, nous croyons que la transparence seule ne résout rien, et peut au contraire légitimer des pratiques d'influence commerciale sur le système de santé. Nous soutenons toute initiative visant à éliminer cette influence commerciale et retrouver un fonctionnement en faveur de l'intérêt public. 

<div class="ts-notes">
    <strong>Transparence défendue par l'Ordre :</strong> 
    <a href="https://www.macsf-exerciceprofessionnel.fr/Responsabilite/Humanisme-deontologie/conflits-interets-professionnels-de-sante-laboratoires">Entretien</a> en mars 2016 à la MACSF du Docteur Patrick BOUET, président du Conseil National de l’Ordre des Médecins : "Nous pensons que face aux doutes et aux interrogations qui circulent actuellement dans le monde de la santé, <strong>la transparence totale est la seule réponse possible. Sans cette transparence, tout devient suspicieux, à juste titre d’ailleurs. Une application totale de la loi Bertrand est donc la seule solution à nos yeux.</strong> Les patients, les associations, les autres professionnels de santé pourront alors connaître la nature des liens d’intérêt d’un médecin et lui poser toutes les questions qu’ils souhaitent à ce sujet. Tant que ces éléments et ces informations ne sont pas accessibles, le doute est de mise."

## Concrètement

Concrètement, le projet EurosForDocs propose les fonctionnalités suivante :

1. nettoyage et enrichissement quotidien de la base Transparence-Santé ;
1. un site web grand public permettant de comprendre le sujet et de visualiser rapidement les liens d'intérêts tissés par les entreprises avec les structures et professionnels bénéficiaires ;
1. un outil d'analyse avancé pour qui souhaite réaliser des études plus approfondies.
`;


function PresentationPage() {
    return (
        <ContentPage title="Objectifs de EurosForDocs" content={pageText} path="/presentation"></ContentPage>
    );
}

export default PresentationPage;
