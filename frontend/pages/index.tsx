import React from "react";
import CustomMarkdown from "../components/custom_markdown";
import Layout from "../components/layout";
import Jumbotron from "react-bootstrap/Jumbotron";
import Container from "react-bootstrap/Container";
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Seo from "../components/seo";

const homePageMarkdown = `
<p style="text-align:center;" class="alert alert-success">
    1er juin 2021 : Notre nouveau site européen est en ligne ! 
    <a href="https://eurosfordocs.eu/"> <strong>eurosfordocs.eu</strong></a> 
     
</p>

<p style="text-align:center;" class="alert alert-danger">
    Le site est actuellement ralenti, et peu utilisable.
    <a href="https://twitter.com/eurosfordocs/status/1472976109570600962?s=20">Explications ici</a>.
</p>

Les études scientifiques et les prescriptions médicales manquent d’indépendance, influencées par une industrie discrètement omniprésente.


Depuis 2010, le scandale du Médiator a amorcé une évolution. 
Les industriels doivent désormais déclarer tous leurs liens d'intérêts financiers dans la base **[Transparence-Santé](https://www.transparence.sante.gouv.fr/)**.

<p style="text-align:center;">
<img 
    src="/images/base_transparence_sante.png" 
    alt="Base Transparence Santé" 
    style="max-width:600px; width: 100%"
/>
</p>

Les déclarations sont extrêmement riches, mais le site public ne permet pas de les explorer facilement.

Au point que **ce dispositif de transparence est généralement perçu comme opaque**. 

L'association **Euros For Docs** simplifie l'accès à la base Transparence-Santé, en nettoyant les données 
et en offrant une interface simplifiée.

## Utiliser EurosForDocs

Avant toute utilisation, merci de lire les **avertissements techniques et légaux**.

<p style="text-align:center;">
     <a class="btn btn-success btn-large" href="/warning" style="padding:15px 30px; margin:10px;">
    Avertissements
    </a>
</p>

Les **fonctionnalités avancées** nécessitent de se connecter à la plateforme.

<p style="text-align:center;">
    <a class="btn btn-success btn-large" href="/explore" style="padding:15px 30px; margin:10px;">
    Fonctionnalités avancées
    </a>
</p>

<p style="text-align:center;">
Voici également une <strong>vision d'ensemble</strong> de la base.
</p>

<p style="text-align:center;">
<a href="https://www.eurosfordocs.fr/metabase/public/dashboard/0931cd9a-de38-4f17-b7d7-b05f56bf3f12">
<img 
    src="/images/global_view_metabase.png" 
    alt="Vision d'ensemble de la base Transparence-Santé"
    style="max-width:800px; width: 100%"
/>
</a>
</p>

<p style="text-align:center;">
<a href="https://twitter.com/eurosfordocs"> 
Nous suivre sur Twitter 
<img 
    src="/images/logo-twitter.png" 
    alt="Logo Twitter"
    style="max-width:100px; width: 100%"
/>
</a>
</p>

`;

const homePageFooterMarkdown = `

---


<p style="text-align:center;">
    <a class="btn btn-success" href="https://frama.link/EFD-Gslides" style="padding:15px 30px; margin:10px;">
    Ouvrir la présentation Google Slides du projet
    </a>
</p>


<p style="text-align:center;">
    <a href="https://frama.link/EFD-Gslides" target="_blank" >
        <img 
            src="/images/accueil_presentation.png"
            alt="Présentation du projet EurosForDocs"
            style="max-width:800px; width: 100%"
        />
    </a>
</p>

`;

enum FeatureRowOrientation {
    left, right
}

const featureRows: FeatureRowProps[] = [
    {
        image: "/images/france_europe.png",
        imageAlt: "Europe",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**1er juin 2021** : [Ouverture du site européen](https://eurosfordocs.eu) 
 
Dans la plupart des autres pays européens, les laboratoire pharmaceutiques ont mis en place d'eux-même un dispositif de transparence. Sous la façade de la transparence, ce dispositif permet surtout de **cacher** beaucoup de liens financiers dans des grandes catégories sans détails. Pour éviter que l'information libérées permettent d'y voir un peu plus clair, elle est dispersée dans des milliers de PDF, sur des milliers de sites internet. 

Ce dispositif de transparence par l'industrie de ses propres pratiques est une hypocrisie, qui a en pratique tout d'une opacité organisée.
Son principal résultat est d'avoir jusqu'à présent évité l'émergence de lois ambitieuses en Europe.

Le nouveau site [eurosfordocs.eu](https://eurosfordocs.eu) collecte ces informations dispersée par l'industrie dans une base unique. L'objectif est de montrer qu'un **Sunshine Act européen** est non seulement nécessaire, mais qu'il serait même techniquement très facile à mettre en oeuvre.
`
    },
    {
        image: "/images/checknews.png",
        imageAlt: "Libération",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**4 janvier 2021** : 418 millions d'euros pour la presse et les médias ?

Les montants très élevés déclarés dans la catégorie *Presse et média* de Transparence-Santé circulent sur les réseaux sociaux, mettant en cause l'indépendance de la presse pour son traitement de la controverse sur l'hydroxychloroquine.

Checknews mène l'enquête, et publie au passage un article de fond présentant l'historique du projet Euros for Docs : 
[Qu'est-ce que la plateforme EurosForDocs, qui recense les liens d'intérêts avec les industriels pharmaceutiques ?](https://www.liberation.fr/checknews/2021/01/04/qu-est-ce-que-la-plateforme-eurosfordocs-qui-recense-les-liens-d-interets-avec-les-industriels-pharm_1809498) ([pdf](download/2021_checknews.pdf))

`
    },
    {
        image: "/images/covid19.png",
        imageAlt: "covid-19",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**Printemps 2020** : Crise sanitaire de la Covid-19.
 
L'influence commerciale joue à plein durant la crise. 
La recherche se concentre massivement sur les solutions médicamenteuses, plutôt que sur [l'épidemiologie de terrain](https://www.mediapart.fr/journal/france/110720/donnees-epidemiologiques-la-penurie-cachee) et les interventions non médicamenteuses.
De nombreux résultats préliminaires sont diffusés via des communiqués de presse d'entreprises, faussant les perceptions du public et les réponses politiques à la pandémie. 
Un exemple clé de ce problème est le Remdesivir de Gilead [[BMJ](https://www.bmj.com/content/369/bmj.m2456)]. 

En France, des observateurs s'appuient sur EurosForDocs pour débattre des potentiels conflits d'intérêts en jeu [Mediapart : [Conseil scientifique](https://www.mediapart.fr/journal/france/310320/covid-19-les-conseillers-du-pouvoir-face-aux-conflits-d-interets), 
[Pr Raoult](https://www.mediapart.fr/journal/france/070420/chloroquine-pourquoi-le-passe-de-didier-raoult-joue-contre-lui?onglet=full)]. 

Promotteur d'un traitement à base d'hydroxychloroquine, le professeur Raoult fait face à des critiques sur la valeur de ses études, et à l'émergence d'un consensus scientifique en sa défaveur [Prescrire [1](https://www.prescrire.org/Fr/203/1845/58630/0/PositionDetails.aspx), [2](https://www.prescrire.org/fr/203/1845/58634/0/PositionDetails.aspx), [3](https://www.prescrire.org/fr/203/1845/58909/0/PositionDetails.aspx), [4](https://www.prescrire.org/fr/3/31/60189/0/NewsDetails.aspx)]. 
Afin d'échapper aux critiques sur le fond, il instrumentalise alors la question des conflits d’intérêts [[Formindep](https://formindep.fr/quelques-lecons-de-la-crise/)] pour faire taire ses détracteurs, 
cherchant à montrer qu'ils sont en conflit d'intérêt avec Gilead,
ignorant les critiques émanant d'acteurs indépendants de l'industrie tels que la Revue Prescrire. 

Auditionné par la commission d'enquête parlementaire, il invite à utiliser EurosForDocs, ce que feront plusieurs médias : 
[Quotidien](https://drive.google.com/file/d/1gbjt9hYYg0YxPs_7INNsBdT7ApTqyPXg/view?usp=sharing),
[20 minutes](https://www.20minutes.fr/societe/2807951-20200626-coronavirus-simple-comme-chou-verifier-liens-entre-medecins-laboratoires-pharmaceutiques-comme-dit-didier-raoult),
[Sciences et Avenir](https://www.sciencesetavenir.fr/sante/enquete-les-infectiologues-francais-sont-ils-trop-proches-de-gilead_145585),
[France Info](https://www.francetvinfo.fr/sante/maladie/coronavirus/conflits-d-interets-le-conseil-scientifique-est-il-lie-aux-laboratoires-pharmaceutiques-comme-le-sous-entend-didier-raoult_4031489.html), 
[AFP](https://factuel.afp.com/les-liens-linterets-entre-labos-et-medecins-le-soupcon-permanent).
`
    },
    {
        image: "/images/pqr.png",
        imageAlt: "PQR",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**11 janvier 2020** : [#TransparenceCHU](https://twitter.com/search?q=transparencechu)

Les journalistes du collectif [DATA+LOCAL](https://collectif-datalocal.github.io/) utilisent EurosForDocs pour mener une enquête sur les liens dangereux entre les CHU et l'industrie. 

Cette enquête est publiée simultanément dans 12 titres de la presse quotidienne régionale, puis relayée dans de nombreux journaux nationaux : [Le Monde](https://www.lemonde.fr/sante/article/2020/01/10/hopitaux-les-chu-ont-recu-170-millions-d-euros-de-laboratoires-pharmaceutiques-en-2018_6025479_1651302.html), [Une du Canard Enchaîné](/images/une_canard.png), etc.

Une [tribune collective dans le Monde](https://www.lemonde.fr/idees/article/2020/02/04/le-terreau-des-conflits-d-interets-a-l-hopital-reste-fertile_6028302_3232.html) propose ensuite 9 mesures pour améliorer la situation des conflits d'intérêts, dont la première est d'améliorer le système Transparence-Santé. 
`
    }, {
        image: "/images/mediapart.png",
        imageAlt: "Logo Mediapart",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**novembre 2019** : Le lobby pharmaceutique avance masqué

Une [tribune collective publiée dans l'Opinion](https://www.lopinion.fr/edition/economie/delais-d-acces-aux-traitements-medicaux-innovants-rattrapons-nos-199941) invite à accélérer la mise sur le marché des médicaments.

Les 86 médecins signataires cumulent [16 millions d'euros de liens d'intérêts déclarés depuis 2013](https://twitter.com/eurosfordocs/status/1194299087547449344?s=20), soit 31000€ par an en moyenne chacun.

[L'enquête menée par Mediapart](https://www.mediapart.fr/journal/france/121119/le-lobby-pharmaceutique-avance-masque) montre que cette tribune a été coordonnée par Agipharm, une association de 14 laboratoires pharmaceutiques américains. 
`
    }, {
        image: "/images/prescrire_point.png",
        imageAlt: "Logo de Prescrire et du Point",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**octobre 2019**

À l'occasion du procès du Mediator, Prescrire publie :   
[La firme Servier, un financeur de poids du monde médical](/download/2019_10_Prescire_La-firme-Servier-un-financeur-de-poids-du-monde-medical.pdf) ([résumé internet](https://www.prescrire.org/fr/3/31/57696/0/NewsDetails.aspx)). 

Cette étude est repris dans un éditorial du Point : [Ces acteurs de la santé qui ont bénéficié des largesses du laboratoire Servier](https://www.lepoint.fr/editos-du-point/anne-jeanblanc/ces-acteurs-de-la-sante-qui-ont-beneficie-des-largesses-du-laboratoire-servier-30-10-2019-2344437_57.php).
`
    }, {
        image: "/images/match_inter.png",
        imageAlt: "Paris Match & France Inter",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**30 avril 2019**

Vaccins anti-HPV : 15 médecins dénoncent des conflits d'intérêts 

En utilisant EurosForDocs, ils démontrent que les signataires d'un appel en faveur d'une généralisation des vaccins anti-HPV
ont reçu 1,6 millions d'euros de financement de la part des industriels produisant ces vaccins.

- [Site du contre-appel](https://spark.adobe.com/page/4cbZuGBONhmDC/)
- [Article de Paris Match](https://www.parismatch.com/Actu/Sante/Vaccins-anti-HPV-15-medecins-denoncent-les-risques-des-conflits-d-interets-1621133)
- [Chronique sur France Inter](https://www.franceinter.fr/emissions/sante-polemique/sante-polemique-07-mai-2019)
`
    }, {
        image: "/images/prescrire.jpg",
        imageAlt: "Logo de Prescrire",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**avril 2019**

La revue médicale indépendante Prescrire rappelle 
l'[utilité des bases de données de type Transparence Santé](/download/2019_04_prescrire.pdf). 

Elle rappelle l'engagement de l'État à améliorer l'accessibilité de la base, et met en avant EurosForDocs.
`
    }, {
        image: "/images/mediapart_mediacites.png",
        imageAlt: "Logo Mediapart et Médiacitées",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**13 mars 2019** : Des victimes d'un médicament anti-calvitie attaquent le laboratoire MSD.

**3 avril 2019** : Des victimes d'un médicament anti-pilosité attaquent le laboratoire Bayer.

Dans des articles publiés sur Mediapart 
([1](https://www.mediapart.fr/journal/france/130319/les-victimes-d-un-traitement-anti-calvitie-poursuivent-le-laboratoire-msd),
[2](https://www.mediapart.fr/journal/france/030419/les-victimes-d-un-medicament-antipilosite-poursuivent-le-laboratoire-bayer)) 
et [Médiacités](https://www.mediacites.fr/lyon/enquete-lyon/2019/04/03/scandale-androcur-les-victimes-du-medicament-anti-pilosite-attaquent-bayer/), 
la journaliste Rozenn Le Saint déchiffre les conflits d'intérêts ayant mené aux mésusages de ces médicaments.

EurosForDocs permet notamment de mesurer l'ampleur des liens d'intérêts entre 
la Société française de dermatologie et le laboratoire MSD d'une part,
et Société française d’endocrinologie et le laboratoire Bayer d'autre part.
`
    }, {
        image: "/images/implant_files_le_monde.jpg",
        imageAlt: "ImplantFiles - Photo d'un TAVI",
        imageOrientation: FeatureRowOrientation.left,
        text: `
**28 novembre 2018**

Les journalistes du Monde ont utilisé EurosForDocs durant l'enquête 
[« Implants médicaux : les industriels sont désormais dans le bloc opératoire »](https://www.lemonde.fr/implant-files/article/2018/11/28/implants-medicaux-les-industriels-sont-desormais-dans-le-bloc-operatoire_5389563_5385406.html).
    
Dans la partie « Des essais cliniques très intéressés », ils montrent comment les experts chargés d'évaluer l'intérêt du TAVI ont été personnellement financés par Medtronic.
`
    } , {
        image: "/images/pharma_papers.jpg",
        imageAlt: "PHARMA PAPERS. Lobbying et mégaprofits. Tout ce que les laboratoires pharmaceutiques voudraient vous cacher",
        imageOrientation: FeatureRowOrientation.right,
        text: `
**13 novembre 2018**
    
C'est notamment grâce à EurosForDocs que les journaux en ligne Basta! et l'Observatoire des multinationales publient les [« Pharma Papers »](https://www.bastamag.net/webdocs/pharmapapers/), une série d'enquêtes sur le lobbying et les mégaprofits des laboratoires pharmaceutiques.
    
EurosForDocs a été [rendu publique](https://www.bastamag.net/webdocs/pharmapapers/l-argent-de-l-influence/eurosfordocs-une-base-de-donnees-d-utilite-publique/) 
    lors de la parution du premier volet : [« L’argent de l’influence »](https://www.bastamag.net/webdocs/pharmapapers/l-argent-de-l-influence/).
    
Cette enquête a fait la [Une](https://www.franceculture.fr/emissions/journal-de-8-h/journal-de-8h-du-mardi-13-novembre-2018) 
    de la matinale de France Culture. 
`
    }
];


interface FeatureRowProps {
    image: string;
    imageAlt: string;
    text: string;
    imageOrientation: FeatureRowOrientation;
}

function FeatureRow({image, imageAlt, text, imageOrientation}: FeatureRowProps) {
    return (
        <div style={{padding: "15px", margin: "20px 0"}}>
            <Row>
                <Col xs={{span: 12, order: 1}}
                     md={{span: 5, order: imageOrientation === FeatureRowOrientation.left ? 1 : 2}}
                     className="ts-feature-row-image">
                    <img src={image} alt={imageAlt} style={{width: '100%', height: '100%', objectFit: 'contain'}}/>
                </Col>
                <Col xs={{span: 12, order: 2}}
                     md={{span: 7, order: imageOrientation === FeatureRowOrientation.left ? 2 : 1}}>
                    <CustomMarkdown source={text}/>
                </Col>
            </Row>
        </div>
    );
}

function HomePage() {
    return (
        <div>
            <Seo title="Accueil"></Seo>
            <Layout>
                <Jumbotron className="ts-hero">
                    <Container>
                        <h1>Bienvenue</h1>
                        <p>
                            Pour la transparence du lobbying des industries de santé.
                        </p>
                    </Container>
                    <span className="ts-hero-caption">
                </span>
                </Jumbotron>
                <div className="container ts-markdown">
                    <Row>
                        <Col md={{offset: 2, span: 8}}>
                            <CustomMarkdown source={homePageMarkdown}/>
                        </Col>
                    </Row>
                    <h2 style={{marginTop: '50px', marginBottom: '30px'}}>Travaux exploitant EurosForDocs</h2>
                    <ListGroup>
                        {featureRows.map(row => {
                            return (
                                <ListGroupItem key={row.image}>
                                    <FeatureRow {...row}></FeatureRow>
                                </ListGroupItem>
                            );
                        })}
                    </ListGroup>
                    <CustomMarkdown source={homePageFooterMarkdown}/>
                </div>
            </Layout>
            <style jsx>{`
                :global(.ts-hero) {
                    background-color: #000;
                    background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url(/images/trust-transparency.jpg);
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: center;
                    position: relative;
                }
                
                :global(.ts-hero) h1 {
                    color: white;
                    font-size: 3em;
                    text-shadow: 1px 1px 4px rgba(0,0,0,0.5);
                }
                
                :global(.ts-hero) p {
                    color: white;
                    font-size: 1.8em;
                    text-shadow: 1px 1px 4px rgba(0,0,0,0.5);
                    padding-top: 20px;
                    padding-bottom: 20px;
                }  
                
                :global(.ts-hero) .ts-hero-caption {
                    position: absolute;
                    bottom: 0;
                    right: 0;
                    margin: 0 auto;
                    padding: 2px 5px;
                    color: #fff;
                    font-family: Georgia,Times,serif;
                    background: #000;
                    text-align: right;
                    z-index: 5;
                    opacity: 0.5;
                    border-radius: 4px 0 0 0;
                } 
                
                .ts-markdown {
                    font-size: 1.2rem;
                }
                
                @media(max-width: 767.98px) {
                    :global(.ts-feature-row-image) {
                        margin-bottom: 30px;
                    }
                }
            `}</style>
        </div>

    );
}


export default HomePage;


