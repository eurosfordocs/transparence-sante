export class Filter {
    constructor(public fieldId: string,
                public fieldName: string,
                public label: string) {
    }
}

export const ALL_SLUGS = ['entreprise_declarante', 'professionnel_beneficiaire', 'structure_beneficiaire', 'annuaire_professionnels'];
export type DashboardSlug = typeof ALL_SLUGS[number];

export function parseSlug(maybeSlug: unknown): DashboardSlug | undefined {
    const slug = ALL_SLUGS.find((validSlug) => validSlug === maybeSlug);
    if (slug !== undefined) {
        return slug;
    }
    return undefined;
}

export class DashboardConfig {
    constructor(public slug: DashboardSlug,
                public uuid: string,
                public id: number,
                public title: string,
                public filters: Filter[]) {
    }
}

export type DashboardConfigs = Map<DashboardSlug, DashboardConfig>;

export const dashboardConfigs: DashboardConfigs = new Map();
dashboardConfigs.set('entreprise_declarante',
    new DashboardConfig(
        'entreprise_declarante',
        'c698a9f3-645f-4c2e-9a04-04baa4906e70',
        227,
        'Vision par Entreprise déclarante',
        [new Filter('134', 'entreprise', 'une entreprise')]));

dashboardConfigs.set('professionnel_beneficiaire',
    new DashboardConfig(
        'professionnel_beneficiaire',
        'a4f5f84e-90c5-4dcd-b497-c57f74d2794a',
        228,
        'Vision par Professionnel bénéficiaire',
        []
    ));

// new Filter('146', 'nom_pr_nom_du_professionel', "un professionnel"),
// new Filter('138', 'identifiant_du_professionel', "un identifiant")

dashboardConfigs.set('structure_beneficiaire',
    new DashboardConfig(
        'structure_beneficiaire',
        'dd4d8702-c473-4baa-abe9-60830c92950b',
        229,
        'Vision par Structure bénéficiaire',
        []
    ));

dashboardConfigs.set('annuaire_professionnels',
    new DashboardConfig(
        'annuaire_professionnels',
        '768c0597-bea8-40e1-84f8-bb9252256b89',
        4,
        'Annuaire des professionels de santé',
        []
    ));
