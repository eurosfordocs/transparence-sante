# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.2
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 3
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython3
#     version: 3.6.5
# ---

# # Setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 70
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100
import matplotlib.pyplot as plt
from matplotlib_venn import venn2, venn3

import sys
ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import ROOTED_CLEANED_DATA_DIR, ROOTED_JOINED_DATA_DIR
import src.constants.csv as csv

DATA_DIR = ROOTED_CLEANED_DATA_DIR
cleaned_remuneration_path = os.path.join(DATA_DIR, csv.REMUNERATION_CSV)
df_remuneration = pd.read_csv(cleaned_remuneration_path, sep=csv.DEFAULT_SEPARATOR)

DATA_DIR = ROOTED_JOINED_DATA_DIR
cleaned_convention_path = os.path.join(DATA_DIR, csv.CONVENTION_CSV)
df_convention = pd.read_csv(cleaned_convention_path, sep=csv.DEFAULT_SEPARATOR)

DATA_DIR = ROOTED_CLEANED_DATA_DIR
cleaned_avantage_path = os.path.join(DATA_DIR, csv.ADVANTAGE_CSV)
df_avantage = pd.read_csv(cleaned_avantage_path, sep=csv.DEFAULT_SEPARATOR)

DATA_DIR = ROOTED_CLEANED_DATA_DIR
cleaned_company_path = os.path.join(DATA_DIR, csv.COMPANY_CSV)
df_company = pd.read_csv(cleaned_company_path, sep=csv.DEFAULT_SEPARATOR)

# # Objectif  : joindre les déclarations au RPPS

df_rpps_path = os.path.join(DATA_DIR, 'deduplicated_rpps.csv')
df_rpps = pd.read_csv(df_rpps_path, sep=';')

from src.specific.functions import get_non_authorized_chars_remover
import string

remove_non_digits = get_non_authorized_chars_remover(string.digits, replacement_char='')

# +
def is_rpps_string(s):
    return len(s)==11 and s.startswith('1')

def clean_rpps(rpps_series):    
    return rpps_series.fillna('').map(remove_non_digits).map(lambda s: int(s) if is_rpps_string(s) else np.nan)
# -

df_remuneration.identifiant = df_remuneration.identifiant.pipe(clean_rpps)

df_convention.identifiant = df_convention.identifiant.pipe(clean_rpps)

df_avantage.identifiant = df_avantage.identifiant.pipe(clean_rpps)

def aggregate_declarations(df, suffix='', additional_agg_list=None):
    additional_agg_list = additional_agg_list or []
    agg_function_dict = {
        'montant_ttc': 'sum', 
        'identifiant': 'count',
    }
    agg_name_dict = {
        'montant_ttc': 'montant' + suffix, 
        'identifiant': 'nombre' + suffix,
    }
    for column, agg_function, agg_name in additional_agg_list:
        agg_function_dict[column] = agg_function
        agg_name_dict[column] = agg_name

    return (df
            .dropna(subset=['identifiant'])
            .groupby('identifiant')
            .agg(agg_function_dict)
            .rename(columns=agg_name_dict)
           )

df_remuneration_agg = aggregate_declarations(df_remuneration, suffix='_remuneration')

df_avantage_agg = aggregate_declarations(df_avantage, suffix='_avantage')

df_convention_agg = aggregate_declarations(
    df_convention, suffix='_convention', 
    additional_agg_list=[('montant_masque', 'sum', 'nombre_convention_sans_montant')]
)

df_convention_agg.columns

def merge_with_aggregated_on_identifiant(df_left, df_aggregated):
    return (df_left.merge(df_aggregated, 
                         how="left", 
                         left_on='identifiant', 
                         right_index=True, 
                         validate='one_to_one',
                         copy=False
                        )
            .fillna({column: 0 for column in df_aggregated.columns})
           )

df_rpps_agg = (df_rpps
 .pipe(merge_with_aggregated_on_identifiant, df_avantage_agg)
 .pipe(merge_with_aggregated_on_identifiant, df_remuneration_agg)
 .pipe(merge_with_aggregated_on_identifiant, df_convention_agg)
)

df_rpps_agg['nombre_liens'] = df_rpps_agg.nombre_avantage + df_rpps_agg.nombre_convention + df_rpps_agg.nombre_remuneration
df_rpps_agg['montant_total'] = df_rpps_agg.montant_avantage + df_rpps_agg.montant_convention + df_rpps_agg.montant_remuneration

agg_columns = ['nombre_liens', 'montant_total', 'nombre_convention_sans_montant']
df_rpps_agg = df_rpps_agg[list(df_rpps.columns) + agg_columns]

# # Joindre les positions GPS sur les communes

df_rpps_agg = df_rpps_agg.dropna(subset=['libellé_commune_structure', 'nom', 'prénom'])

from src.specific.functions import normalize_city

gps_columns = ['latitude', 'longitude']
poste_columns = ['Nom_commune'] + gps_columns

df_poste = (pd.read_csv("data/raw/laposte_hexasmal.csv", sep=';')
            .dropna(subset=['Nom_commune','coordonnees_gps'])
           )
df_poste.Nom_commune = df_poste.Nom_commune.pipe(normalize_city)
df_poste = df_poste[~df_poste.Nom_commune.duplicated(keep='first')]

df_gps_columns = pd.DataFrame(
    data=df_poste.coordonnees_gps.str.split(',').tolist(), 
    columns=gps_columns,
    index=df_poste.index
)
df_poste = pd.concat([df_poste, df_gps_columns], axis='columns')[poste_columns]

df_main_cities_poste = pd.DataFrame([
    ('Paris', '48.8534', '2.3488'),
    ('Marseille', '43.3', '5.4'),
    ('Lyon', '45.75', '4.85')],
    columns=poste_columns
)

df_poste = pd.concat([df_main_cities_poste, df_poste]).reset_index(drop=True)

# 2nd fichier inutile pour l'instant

df_circo = pd.read_csv("data/raw/EUCircos_Regions_departements_circonscriptions_communes_gps.csv", sep=';')
df_circo = df_circo.dropna(subset=['latitude', 'longitude'])
df_circo.nom_commune = df_circo.nom_commune.pipe(normalize_city)
print(df_circo.shape)
df_circo.sample()

venn3([set(df_poste.Nom_commune), set(df_circo.nom_commune), set(df_rpps_agg.libellé_commune_structure)], 
      set_labels=["communes poste", "communes circo", "communes rpps"]);

df_rpps_agg.libellé_commune_structure.isin(set(df_poste.Nom_commune)).value_counts()

# Aggregation

assert df_rpps_agg.libellé_commune_structure.isna().sum() == 0

assert df_poste.Nom_commune.isna().sum() == 0

assert df_poste.Nom_commune.duplicated().sum() == 0

df_rpps_agg_poste = (df_rpps_agg
                     .merge(
                         df_poste, 
                         how='inner',
                         left_on='libellé_commune_structure', 
                         right_on='Nom_commune',
                         validate='many_to_one'
                     )
                     .drop(columns=['Nom_commune'])
                    )

df_rpps_agg_poste.rename(columns={'libellé_commune_structure': 'commune'}, inplace=True)

df_rpps_agg_poste.head(3)

df_rpps_joined_path = os.path.join(ROOTED_JOINED_DATA_DIR, 'rpps.csv')
df_rpps_agg_poste.to_csv(df_rpps_joined_path, sep=';', index=False)

(df_rpps_agg_poste[df_rpps_agg_poste.profession == 'Médecin'].nombre_liens == 0).mean()

df_rpps_agg_poste.identifiant.duplicated().sum()
