# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

cd ..

cd docker/nginx/nginx-logs/

import hashlib

with open("access.log", 'r') as f_in, open("access-private.log", 'w') as f_out:
    for line in f_in:
        ip, log = line.split(' ', maxsplit=1)
        hashed_ip = hashlib.sha224(ip.encode("utf8")).hexdigest()
        f_out.write(f"{hashed_ip} {log}")

import pandas as pd

pd.options.display.max_colwidth = 100

df = pd.read_csv("access-private.log",
                 sep=r'\s(?=(?:[^"]*"[^"]*")*[^"]*$)(?![^\[]*\])',
                 engine='python',
                 usecols=[0, 3, 4, 5, 6, 7, 8, 10],
                 names=['ip', 'time', 'request', 'status', 'size', 'referer', 'user_agent', 'request_time'],
                 na_values=['-', '"-"'],
                 header=None
                 )

df.status.value_counts()

df[df.status != 200]
