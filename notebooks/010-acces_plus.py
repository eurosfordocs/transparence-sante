# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
# ---

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 30
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100

ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import CLEANED_DATA_DIR, RAW_DATA_DIR
import src.constants.csv as csv

annuaire = os.path.join(CLEANED_DATA_DIR, csv.HEALTH_PROFESSIONAL_CSV)
df = pd.read_csv(annuaire, sep = ';')

df.head()

identifiants = pd.read_clipboard(sep='\t')

identifiants.to_csv(RAW_DATA_DIR + "/identifiants_acces_plus.csv")

lignes = pd.read_clipboard(sep='\t')

df_id_annuaire = identifiants.merge(df, on='identifiant', how="left")

lignes_id_annuaire = lignes.merge(df_id_annuaire, on='nom_prénom', how='left')[['nom_prénom','identifiant', "civilite", 'commune', 'profession', 'specialité']]

lignes_id_annuaire.to_clipboard(index=False, sep='\t')

lignes.merge(df_id_annuaire, on='nom_prénom', how='left')


