# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 100
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100
from pprint import pprint

import sys
ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import ROOTED_CLEANED_DATA_DIR, ROOTED_RAW_DATA_DIR
import src.constants.csv as csv

from src.utils.postgres import create_postgres_engine, execute_text_query
from src.constants import column, tables

from src.api import api

engine = create_postgres_engine()

raw_query = api.TEMPLATE_SEARCH_HEALTH_PROFESSIONAL_PREFIX.format(prefix='ab')

# +
result_proxy = execute_text_query(engine, raw_query)

list_of_row_dict = []
for row_proxy in result_proxy:
    list_of_row_dict.append(dict(row_proxy))
pprint(list_of_row_dict)
# -

dict(rowproxy)

column.__dict__












