# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.2
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 3
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython3
#     version: 3.6.5
# ---

# # Initialisation

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
from pprint import pprint
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 70
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100
import matplotlib.pyplot as plt
from matplotlib_venn import venn2

ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import CLEANED_DATA_DIR, RAW_DATA_DIR
import src.constants.csv as csv

from src.specific.functions import get_non_authorized_chars_remover, replace_accents
import string

# # Lecture des données

# +
DATA_DIR = CLEANED_DATA_DIR

path = os.path.join(DATA_DIR, csv.HEALTH_DIRECTORY_CSV)
df = pd.read_csv(path, sep=csv.DEFAULT_SEPARATOR)
# -

# ### Coup d'oeil

columns = [
    'identifiant',
    'civilité_exercice',
    'nom',
    'prénom',
    'profession',
    'catégorie_professionnelle',
    'specialité',
    'type_spécialité',
    'raison_sociale_site',
    'enseigne_commerciale_site',
    'identifiant_structure',
    'finess_etablissement_juridique',
    'finess_site',
    'siren_site',
    'siret_site',
    'complément_destinataire_structure',
    'complément_point_géographique_structure',
    'numéro_voie_structure',
    'indice_répétition_voie_structure',
    'libellé_type_de_voie_structure',
    'libellé_voie_structure',
    'libellé_commune_structure',
    'code_postal_structure',
    'bureau_cedex_structure',
    'mention_distribution_structure',
    'libellé_pays_structure',
    'adresse_email_structure',
    'adresse_bal_mssanté',
    'télécopie_structure',
    'téléphone_2_structure',
    'téléphone_structure'
]


name_columns = [
    'nom',
    'prénom',
]
id_name_columns = ['identifiant'] + name_columns

df.shape

df[columns].head(3)

# Les médecin sans spécialité sont des étudiants

df[df.type_spécialité.isna() & (df.profession == 'Médecin')].catégorie_professionnelle.value_counts()

df[df.specialité == 'Médecine Générale'][df.catégorie_professionnelle == 'Civil'].identifiant.nunique()

# # Objectif
#
# 1. Créer un identifant "lisible" type nom + prénom + spécialité + lieu + rpps => pour la recherche
# 2. Dédoublonner à 1 ligne par RPPS

# # Dédoublonage

# ### Préparation des colonnes à dédoublonner

# +
sorted_profession = [
    'Pédicure-Podologue',
    'Masseur-Kinésithérapeute',
    'Sage-Femme',
    'Chirurgien-Dentiste',
    'Pharmacien',
    'Médecin',
]

df['profession'] = pd.Categorical(
    df['profession'], 
    categories=sorted_profession, 
    ordered=True
)
# -

def get_sorted_categorical_by_frequency(df, column, max_is_rarest=True):
    # if max_is_rarest : 
    # - Rarest value will be the max in categorical order, 
    # - min value will be NaN, 2nd min value will be the most frequent
    # else : 
    # - Most frequent value will be the max in categorical order, 
    # - min value will be NaN, 2nd min value will be the rarest

    is_ascending = not max_is_rarest
    values_sorted_by_descending_frequency = df[column].value_counts().sort_values(ascending=is_ascending).index
    return pd.Categorical(
        df[column], 
        categories=values_sorted_by_descending_frequency, 
        ordered=True
    )


df['specialité'] = get_sorted_categorical_by_frequency(df, 'specialité')

# +
def normalize_code_postal(code):
    try:
        result = str(int(code)).zfill(5)
    except ValueError:
        result = np.nan
    return result

df['code_postal_structure'] = df.code_postal_structure.map(normalize_code_postal)
df['code_postal_structure'] = get_sorted_categorical_by_frequency(df, 'code_postal_structure', max_is_rarest=False)
# -

from src.specific.functions import normalize_city

df['libellé_commune_structure'] = df.libellé_commune_structure.pipe(normalize_city)
df['libellé_commune_structure'] = get_sorted_categorical_by_frequency(df, 'libellé_commune_structure')

# ### Dédoublonnage

# %%time
df_treated = df.groupby("identifiant", sort=False, as_index=False).agg({
    'nom': max,
    'prénom': max,
    'profession': max,
    'specialité': max,
#    'code_postal_structure': max,
    'libellé_commune_structure': max,
})

df_treated.isna().sum()

df_treated_path = os.path.join(DATA_DIR, 'deduplicated_rpps.csv')
df_treated.to_csv(df_treated_path, sep=';', index=False)

# ### Étude résultat

assert df_treated.identifiant.duplicated().sum() == 0

df_treated.shape

# #### Nombre d'homonymes

df_treated.duplicated(subset=name_columns).sum()

# #### Nombre de médecins généralistes

df_treated[df_treated.specialité == 'Médecine Générale'].shape

# #### Nombre de doublons en ajoutant la spécialité

df_treated.duplicated(subset=name_columns + ['specialité']).sum()

# #### Nombre de doublons en ajoutant la spécialité ET la commune

df_treated.duplicated(subset=name_columns + ['specialité', 'libellé_commune_structure']).sum()

# #### Visualisation des doublons "ultimes", qui ont un code postal et une structure

df_treated[df_treated.duplicated(subset=name_columns + ['specialité', 'libellé_commune_structure'])].dropna()

# # Création d'un identifiant textuel unique pour la recherche

df_treated.identifiant.map(str).str[0].value_counts()







# # Analyse et tests

# Fonction pour analyser les doublons

def get_subset_multiple_values_same_id(column):
    n_values_of_column_for_id = df.groupby("identifiant")[column].nunique()
    id_with_multiple_value_of_column = set(n_values_of_column_for_id[n_values_of_column_for_id > 1].index)
    mask_multiple_names = df.identifiant.isin(id_with_multiple_value_of_column)
    df_multiple_values_same_id = df[mask_multiple_names].drop_duplicates(subset=["identifiant", column])
    print(df_multiple_values_same_id.shape[0])
    return (df_multiple_values_same_id.sort_values('identifiant'))

# 0 doublons complet

assert df.duplicated().sum() == 0

# Nombreux doublons sur l'identifiant

df.duplicated(subset=['identifiant']).value_counts()

# ## Plusieurs noms pour le même identifiant x72 lignes
# Solution : 
# - Bonne : récupérer le set des nom, et les concaténer
# - Réalisé : prendre le nom le plus 'grand' au sens lexicographique

get_subset_multiple_values_same_id("nom")[id_name_columns].head(10)

# ### Traitement

# %time df_nom = df.groupby('identifiant').nom.max()

df_nom[10000107523]

df_nom[10001392330]

# ## Plusieurs prénoms  x32 lignes
# Même solutions que nom

get_subset_multiple_values_same_id("prénom")[id_name_columns].head(10)

# ## Traitement

# %time df_prénom = df.groupby('identifiant').prénom.max()

# ## Plusieurs catégories professionnelles x1082 lignes

(get_subset_multiple_values_same_id("catégorie_professionnelle")
[id_name_columns + ["profession", "catégorie_professionnelle"]]
 .head(10))

# ## Plusieurs professions x 152 lignes
# Faire un choix arbitraire, ou selon une hiérarchie (catégorie avec plus de liens devant), ou selon distinction étudiant-civil

(get_subset_multiple_values_same_id("profession")
[id_name_columns + ["profession", "catégorie_professionnelle"]]
 .head(10))

# ### Traitement

# +
sorted_profession = [
    'Pédicure-Podologue',
    'Masseur-Kinésithérapeute',
    'Sage-Femme',
    'Chirurgien-Dentiste',
    'Pharmacien',
    'Médecin',
]

df['profession'] = pd.Categorical(
    df['profession'], 
    categories=sorted_profession, 
    ordered=True
)
# -

# %%time 
df_pro = df.groupby("identifiant", sort=False, as_index=False).agg({
    'profession': max
})

df_pro.groupby('identifiant').profession.nunique(dropna=False).value_counts()

# ## Plusieurs spécialités x469 lignes
# - Regroupement des spé de med gé
# - pharmacien -> le plus spécifique "Expérience prat. art. R.5124-16 du CSP Exploitant" plutôt que "Fabriquant"
# - médecin -> le plus spécifique (non med gé)
#

# +
(get_subset_multiple_values_same_id("specialité")
[id_name_columns + ["specialité", "catégorie_professionnelle", "raison_sociale_site", 'enseigne_commerciale_site']]
[df.profession != 'Médecin']

)
# -

# ### Traitement

# %%time 
df_spe = df.groupby("identifiant", sort=False, as_index=False).agg({
    'specialité': max
})

# ## Code postal

# ### Fichiers de codes postaux avec des coordonnées géographiques

# #### poste

df_poste = pd.read_csv("data/raw/laposte_hexasmal.csv", sep=';')

print(df_poste.shape)
df_poste.sample(2)

df_poste.Code_commune_INSEE.map(str).map(len).value_counts()

codes_postaux_poste = set(df_poste.Code_postal.map(str))
codes_insee_poste = set(df_poste.Code_commune_INSEE.map(str))

# #### EU Circos

df_circo = pd.read_csv("data/raw/EUCircos_Regions_departements_circonscriptions_communes_gps.csv", sep=';')
df_circo = df_circo.dropna(subset=['latitude', 'longitude'])

print(df_circo.shape)
df_circo.sample(2)

codes_postaux_circo = set(df_circo.codes_postaux)
codes_insee_circo = set(df_circo.code_insee.map(str))

# #### Correspondance insee postal

df_corres = pd.read_csv("data/raw/correspondances-code-insee-code-postal.csv", sep=';')

print(df_corres.shape)
df_corres.sample(2)

df_corres.Commune.nunique()

# #### Intersection entre les ensembles

code_postaux_corres = set(df_corres['Code Postal'])
code_insee_corres = set(df_corres['Code INSEE'])

venn2([codes_postaux_poste, code_postaux_corres], set_labels=["poste", "corres"]);

venn2([codes_postaux_poste, codes_postaux_circo], set_labels=["codes postaux poste", "codes postaux circo"]);

venn2([codes_insee_poste, codes_insee_circo], set_labels=["codes insee poste", "codes insee circo"]);

codes_postaux = codes_postaux_poste | codes_postaux_circo

codes_insee = codes_insee_poste | codes_insee_circo

# #### Choix final

codes = codes_postaux | codes_insee

# ### Normalisation code 

# %%time 
df_code = df.groupby("identifiant", sort=False, as_index=False).agg({
    'code_postal_structure': max
})

df_code = df.code_postal_structure.map(normalize_code_postal)
df_code = df_code[df_code.map(len) > 0]

df_code.isin(codes).value_counts()

df['code_postal_in_codes_postaux'] = df.code_postal_structure.map(normalize_code_postal).isin(codes_postaux)
df['code_postal_in_codes'] = df.code_postal_structure.map(normalize_code_postal).isin(codes)

(get_subset_multiple_values_same_id("code_postal_structure")
[id_name_columns + ["code_postal_structure"]]
)

# ## commune

df_poste.Nom_commune.nunique()

df_treated.libellé_commune_structure.nunique()


# #### Lien pour coordonnées GPS

df_annuaire_commune = df.libellé_commune_structure.pipe(normalize_city)
set_annuaire_commune = set(df_annuaire_commune)
print(df.libellé_commune_structure.nunique(dropna=False), len(set_annuaire_commune))

set_annuaire_commune = set(df_treated.libellé_commune_structure)

df_circo_commune = df_circo.nom_commune.pipe(normalize_city)
set_circo_commune = set(df_circo_commune)
print(df_circo.nom_commune.nunique(dropna=False), len(set_circo_commune))

df_poste_commune = df_poste.Nom_commune.pipe(normalize_city)
set_poste_commune = set(df_poste_commune)
print(df_poste.Nom_commune.nunique(dropna=False), len(set_poste_commune))

venn2([set_circo_commune, set_poste_commune], ('communes circo', 'communes poste'));

set_communes = set_circo_commune | set_poste_commune | set(main_cities)

venn2([set_annuaire_commune, set_communes], ('communes annuaire', 'communes poste & autres'));

df_annuaire_commune.isna().value_counts()

mask_commune = df_annuaire_commune.dropna().isin(set_communes)
mask_commune.value_counts()

df_annuaire_commune.dropna()[~mask_commune].value_counts().head(10)

from difflib import get_close_matches

get_close_matches('La Hague', set_poste_commune, cutoff=0.8)

df['libellé_commune_structure'] = df.libellé_commune_structure.pipe(normalize_city)
df['libellé_commune_structure'] = get_sorted_categorical_by_frequency(df, 'libellé_commune_structure')

(get_subset_multiple_values_same_id("libellé_commune_structure")
[id_name_columns + ["libellé_commune_structure"]]
 .head(10))

# %%time 
df_com = df.groupby("identifiant", sort=False, as_index=False).agg({
    'libellé_commune_structure': max
})

df_com.groupby('identifiant').libellé_commune_structure.nunique(dropna=False).value_counts()




