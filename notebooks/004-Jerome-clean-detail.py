# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 3
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython3
#     version: 3.6.7
# ---

# # Setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

# +
import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 100
pd.options.display.max_columns = 100

pd.options.display.max_colwidth = 100
# -

import sys
ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import ROOTED_CLEANED_DATA_DIR, ROOTED_RAW_DATA_DIR
import src.constants.csv as csv

DATA_DIR = ROOTED_RAW_DATA_DIR

# %%time
avantage_path = os.path.join(DATA_DIR, csv.ADVANTAGE_CSV)
df_avantage = pd.read_csv(avantage_path, sep=csv.DEFAULT_SEPARATOR, usecols=['avant_nature'])

print(df_avantage.shape)
df_avantage.sample(2)

# # Objectif  : Nettoyer la colonne détail
#
# Problème:
# - La nature des avantages est en champ libre des déclarations
# - Il n'y a pas de normalisation de la nature des avantages
#
# Résultat attendu :
# - se ramener à un certain nombre de modalités pour la nature des avantages (avant_nature)
#
# Modalités de la classification :
#

"repas"
"repas déjeuner"
"repas RP"
"transport"
"hospitalite"
"hebergement"
"enquete"
"participation even scientif"
"inscription congrès"
"repas staff"
"cadeaux"
"dons de fonctionnement"
"remuneration indemnite"
"invitation"
"dons de materiel"
"remboursement frais"
"formation ESSEC"

df_avantage.avant_nature.value_counts().head(20)

from src.specific.declaration.functions import replace_accents, replace_special_characters

s_nature = (df_avantage.avant_nature
 .map(replace_accents)
 .map(replace_special_characters)
 .str.upper()
 .str.replace('AUTRE', ' ')
 .str.split().str.join(' ')
)


categories = [
    'HOSPITALITE',
    'REPAS',
    'HEBERGEMENT',
    'TRANSPORT',
    'PARTICIPATION EVENEMENT',
    'CADEAUX',
    'REMUNERATION',
    'DONS'
]

# +
mapping_categorie_to_avantages = {
    'HEBERGEMENT RESTAURATION TRANSPORT': [
        'HEBERGEMENT RESTAURATION TRANSPORT',
           
    ],
    'HOSPITALITE' : [
        'HOSPITALITE',
        'FRAIS D HOSPITALITE',
        'HOSPITALITE REUNION PROFESSIONNELLE',
    ],
    'REPAS': [
        'REPAS', 
        'RESTAURATION', 
        'FRAIS DE BOUCHE',
        'INVITATION REPAS',
        'REPAS PROFESSIONNEL',
        'REPAS PROFESSIONNELS',
        'FRAIS DE REPAS',
        
        'DINER',
        'INVITATION A DEJEUNER DINER',
        'RESTAURATION DINER',
        'DINER REPAS',
        
        'FORMATION MEDICALE CONTINUE',
        
        'REPAS REUNION SCIENTIFIQUE',
        
        'REPAS REUNION', 
        'REPAS MANIFESTATION DE FORMATION',
        'FRAIS DE RESTAURATION RP',
        'FRAIS D HOSPITALITE RP',
        'DINER REUNION',
        'RP',
        
        'DEJEUNER REUNION',
        
        'REPAS IMPROMPTU',
        'DEJEUNER',
        'DEJEUNER REPAS',
        'RELATIONS NORMALES DE TRAVAIL',
        'RNT',
        'REPAS RELATIONS NORMALES DE TRAVAIL',
        'REPAS RELATION NORMALE DE TRAVAIL',
        'REPAS RNT',
        'REPAS DE TRAVAIL',
        'DEJEUNER D OPPORTUNITE',
        'REPAS D AFFAIRE',
        
        'STAFF',
        'RESTAURATION STAFF',
        'REPAS STAFF',
        'HOSPITALITE STAFF',
        'REPAS REUNION HOSPITALIERE',
        'COLLATION STAFF',
        'STAFF REPAS',
        
        'RESTAURATION BUFFET',
        'TRAITEUR',
    ],
    'TRANSPORT' : [
        'TRANSPORT',
        'FRAIS DE DEPLACEMENT',
        'FRAIS DE DA A PLACEMENT',
        'FRAIS DE TRANSPORT',
        'ACHEMINEMENT',
        'VOYAGES',
        'FRAIS REMBOURSES TRANSPORT',
         
    ],
    'HEBERGEMENT' : [
        'HEBERGEMENT',
        'HA BERGEMENT',
        'HA A BERGEMENT',
        'HOTEL',
        'HE BERGEMENT',
        'HEBERGEMENT SUR TOUTE LA DUREE DU CONGRES',
        'HOTEL HEBERGEMENT',
        'H BERGEMENT',
    ],
    'PARTICIPATION CONGRES/SYMPOSIUM' : [
        'PARTICIPATION EVENEMENT',
        'PARTICIPATION EVENEMENT SCIENTIFIQUE',
        'FRAIS DE CONGRES',
        'CONGRES',
        'REPAS CONGRES',
        
        'INSCRIPTION', 
        'FRAIS D INSCRIPTION',
        'INSCRIPTIONS',
        'INVITATIONS INSCRIPTIONS',
        
        'INVITATIONS',
        'INVITATION',
        
        'INSCRIPTION EVENEMENT SCIENTIFIQUE',
        'INSCRIPTION CONGRES',
        'INSCRIPTION CONGRES SCIENTIFIQUE',
        'FRAIS D INSCRIPTION CONGRES',
        
        'HOSPITALITE EVENEMENT SCIENTIFIQUE',
        'HOSPITALITE CONGRES SYMPOSIUM',
        
        'HOSPITALITE INSCRIPTION',
        'HOSPITALITE CONGRES',
        
    ],
    'REMUNERATION': [
        'ENQUETE',
        'HONORAIRES ETUDES',
        'ENQUETE TELEPHONIQUE',
        'ENQUETE WEB',
        'ENQUETE INTERNET',
        'HONORAIRES',
        'HONORAIRE',
        'HONORAIRES MEDECIN INVESTIGATEUR',
    ],
    'DONS': [
        'DONS DE FONCTIONNEMENT',
        'DON DE FONCTIONNEMENT',
        'DONS DE MATERIEL',
        'DON DE MATERIEL',
        'ECHANTILLON',
        'DON',
        
        'SUBVENTION',
    ],
    'CADEAUX': [
        'CADEAUX',
        'CADEAU',
        'LIVRE',
        'CHAMPAGNE',
        'BOITE DE CHOCOLAT',
    ],
    'REMBOURSEMENT FRAIS SANS PRECISION': [
        'REMBOURSEMENT DE FRAIS',
        'DEDOMMAGEMENT',
        'FRAIS DIVERS',
        
        'REMBOURSEMENT FRAIS LOGISTIQUE',
        
    ],
    'FORMATION' : [
        'FORMATION',
        'FRAIS DE FORMATION',
        
    ],
    
}
# -


mapping_avantage_to_categorie = dict()
for categorie, list_avantage in mapping_categorie_to_avantages.items():
    for avantage in list_avantage:
        mapping_avantage_to_categorie[avantage] = categorie



# +
def map_to_categorie(avantage):
    if avantage in mapping_avantage_to_categorie:
        return mapping_avantage_to_categorie[avantage]
    else:
        return avantage

s_nature_mapped = s_nature.map(map_to_categorie)
# -

list(s_nature_mapped.value_counts().head(100).index)
#s_nature_mapped.value_counts().head(100)


df.dtypes


