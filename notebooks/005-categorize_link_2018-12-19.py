# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 100
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100
from pprint import pprint

import sys
ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import ROOTED_CLEANED_DATA_DIR, ROOTED_RAW_DATA_DIR
import src.constants.csv as csv

# # Load data 

DATA_DIR = ROOTED_RAW_DATA_DIR
NROWS=10**5

# %%time
csv_path = os.path.join(DATA_DIR, csv.ADVANTAGE_CSV)
df_avantage = pd.read_csv(csv_path, sep=csv.DEFAULT_SEPARATOR, nrows=NROWS, 
                          usecols=['avant_nature'])

# %%time
csv_path = os.path.join(DATA_DIR, csv.CONVENTION_CSV)
df_convention = pd.read_csv(csv_path, sep=csv.DEFAULT_SEPARATOR, nrows=NROWS, 
                            usecols=['conv_objet', 'conv_objet_autre'])

# # Preprocess detail

import string
from src.specific.functions import normalize_detail
from src.specific.declaration.convention import convention_object

df_convention['detail'] = convention_object(df_convention)

df_avantage.avant_nature.value_counts().head()

df_convention['detail'].value_counts().head()

# %%time
df_avantage['normalized_detail'] = df_avantage.avant_nature.pipe(normalize_detail)


# %%time
df_convention['normalized_detail'] = df_convention['detail'].pipe(normalize_detail)

# # Map to ontology

from src.constants import ontology
ontology.print_tree()

DICT_CATEGORY_TO_MODALITIES = {
    ontology.TRAVEL_AND_HOSPITALITY: [
        'HEBERGEMENT RESTAURATION TRANSPORT',
           
    ],
    ontology.HOSPITALITY : [
        'HOSPITALITE',
        'FRAIS D HOSPITALITE',
        'HOSPITALITE REUNION PROFESSIONNELLE',
    ],
    ontology.MEAL_AND_DRINK: [
        'REPAS', 
        'RESTAURATION', 
        'FRAIS DE BOUCHE',
        'INVITATION REPAS',
        'REPAS PROFESSIONNEL',
        'REPAS PROFESSIONNELS',
        'FRAIS DE REPAS',
        
        'DINER',
        'INVITATION A DEJEUNER DINER',
        'RESTAURATION DINER',
        'DINER REPAS',
        
        'FORMATION MEDICALE CONTINUE',
        
        'REPAS REUNION SCIENTIFIQUE',
        
        'REPAS REUNION', 
        'REPAS MANIFESTATION DE FORMATION',
        'FRAIS DE RESTAURATION RP',
        'FRAIS D HOSPITALITE RP',
        'DINER REUNION',
        'RP',
        
        'DEJEUNER REUNION',
        
        'REPAS IMPROMPTU',
        'DEJEUNER',
        'DEJEUNER REPAS',
        'RELATIONS NORMALES DE TRAVAIL',
        'RNT',
        'REPAS RELATIONS NORMALES DE TRAVAIL',
        'REPAS RELATION NORMALE DE TRAVAIL',
        'REPAS RNT',
        'REPAS DE TRAVAIL',
        'DEJEUNER D OPPORTUNITE',
        'REPAS D AFFAIRE',
        
        'STAFF',
        'RESTAURATION STAFF',
        'REPAS STAFF',
        'HOSPITALITE STAFF',
        'REPAS REUNION HOSPITALIERE',
        'COLLATION STAFF',
        'STAFF REPAS',
        
        'RESTAURATION BUFFET',
        'TRAITEUR',
    ],
    ontology.TRAVEL : [
        'TRANSPORT',
        'FRAIS DE DEPLACEMENT',
        'FRAIS DE DA A PLACEMENT',
        'FRAIS DE TRANSPORT',
        'ACHEMINEMENT',
        'VOYAGES',
        'FRAIS REMBOURSES TRANSPORT',
         
    ],
    ontology.ACCOMMODATION : [
        'HEBERGEMENT',
        'HA BERGEMENT',
        'HA A BERGEMENT',
        'HOTEL',
        'HE BERGEMENT',
        'HEBERGEMENT SUR TOUTE LA DUREE DU CONGRES',
        'HOTEL HEBERGEMENT',
        'H BERGEMENT',
    ],
    ontology.EVENT : [
        'PARTICIPATION EVENEMENT',
        'PARTICIPATION EVENEMENT SCIENTIFIQUE',
        'FRAIS DE CONGRES',
        'CONGRES',
        'REPAS CONGRES',
        
        'INSCRIPTION', 
        'FRAIS D INSCRIPTION',
        'INSCRIPTIONS',
        'INVITATIONS INSCRIPTIONS',
        
        'INVITATIONS',
        'INVITATION',
        
        'INSCRIPTION EVENEMENT SCIENTIFIQUE',
        'INSCRIPTION CONGRES',
        'INSCRIPTION CONGRES SCIENTIFIQUE',
        'FRAIS D INSCRIPTION CONGRES',
        
        'HOSPITALITE EVENEMENT SCIENTIFIQUE',
        'HOSPITALITE CONGRES SYMPOSIUM',
        
        'HOSPITALITE INSCRIPTION',
        'HOSPITALITE CONGRES',
        
    ],
    ontology.REMUNERATION: [
        'ENQUETE',
        'HONORAIRES ETUDES',
        'ENQUETE TELEPHONIQUE',
        'ENQUETE WEB',
        'ENQUETE INTERNET',
        'HONORAIRES',
        'HONORAIRE',
        'HONORAIRES MEDECIN INVESTIGATEUR',
    ],
    ontology.CASH_DONATION: [
        'DONS DE FONCTIONNEMENT',
        'DON DE FONCTIONNEMENT'
    ],
    ontology.DONATION_IN_KIND: [
        'DONS DE MATERIEL',
        'DON DE MATERIEL'
        'ECHANTILLON',
    ],
    ontology.DONATION: [
        'DON',
    ],
    ontology.GRANT: [
        'SUBVENTION',
    ],
    ontology.GIFT: [
        'CADEAUX',
        'CADEAU',
        'LIVRE',
        'CHAMPAGNE',
        'BOITE DE CHOCOLAT',
    ],
    ontology.RELATED_EXPENSE: [
        'REMBOURSEMENT DE FRAIS',
        'DEDOMMAGEMENT',
        'FRAIS DIVERS',
        
        'REMBOURSEMENT FRAIS LOGISTIQUE',
        
    ],
    ontology.TRAINING : [
        'FORMATION',
        'FRAIS DE FORMATION',
    ],
    ontology.EMPTY_AND_OTHER : [
        ''
    ]
}

# ## Get strict mapper

def get_mapper_to_category():
    class Mapper(dict):
        def __missing__(self, key):
            return ontology.UNSUCESSFULL_MAPPING.name_fr
    
    mapping = Mapper()
    for category, list_modality in DICT_CATEGORY_TO_MODALITIES.items():
        for modality in list_modality:
            mapping[modality] = category.name_fr
    return mapping

map_modality_to_category = get_mapper_to_category()

# %%time
df_avantage['categorie_strict'] = df_avantage.normalized_detail.map(map_modality_to_category)

# ## Get similarity mapper


from difflib import get_close_matches

def get_reversed_dict(value_to_list):
    mapping = dict()
    for value, list_ in value_to_list.items():
        for list_element in list_:
            mapping[list_element] = value
    return mapping

modality_to_category = get_reversed_dict(DICT_CATEGORY_TO_MODALITIES)

normalized_detail = "DA JEUNER"
normalized_detail = 'RSTAURATION'
modalities = list(modality_to_category.keys())
get_close_matches(normalized_detail, modalities, n=1, cutoff=0.8)



def get_mapper_to_category():
    unique_details = df_avantage.normalized_detail.unique()
    modalities = list(modality_to_category.keys())
    
    mapping = dict()
    for normalized_detail in unique_details:
        match = get_close_matches(normalized_detail, modalities, n=1, cutoff=0.8)
        if match:
            mapping[normalized_detail] = modality_to_category[match[0]].name_fr
        else:
            mapping[normalized_detail] = ontology.UNSUCESSFULL_MAPPING.name_fr
    return mapping

map_modality_to_category = get_mapper_to_category()
map_modality_to_category["RSTAURATION"]

# %%time
df_avantage['categorie_similarity'] = df_avantage.normalized_detail.map(map_modality_to_category)

# ## Apply mapper

is_mapping_sucessfull = df_avantage["categorie_similarity"] != ontology.UNSUCESSFULL_MAPPING.name_fr

is_mapping_strict_sucessfull = df_avantage["categorie_strict"] != ontology.UNSUCESSFULL_MAPPING.name_fr

df_avantage.head()

df_avantage[~is_mapping_sucessfull].drop_duplicates().head()

df_avantage[~is_mapping_strict_sucessfull].drop_duplicates().head(20)

(df_avantage[~is_mapping_sucessfull]
 .normalized_detail
 .value_counts()
 .head(20)
)

df_avantage[df_avantage.normalized_detail == "DA JEUNER"].drop_duplicates()


